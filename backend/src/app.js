// Import required modules
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const userRoutes = require('./routes/user_route');
const vehicleRoutes = require('./routes/vehicle_route');
const orderRoutes = require('./routes/order_route'); 
const rentACarObjectRoutes = require('./routes/rentACarObject_route');
const path = require('path');
require('dotenv').config();

// Create an instance of Express
const app = express();

app.use('/data', express.static(path.join(__dirname, 'data')));
app.use(cors());
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));

// Routes
app.use('/api', userRoutes);
app.use('/api', vehicleRoutes);
app.use('/api', orderRoutes);
app.use('/api', rentACarObjectRoutes);


// Start the server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
