const RentACarObjectModel = require('../models/rentACarObject_model');


// Create a new rent-a-car object
function createRentACarObject(req, res) {
  const rentACarObjectData = req.body; 
  try {
    
    const newObject = RentACarObjectModel.createRentACarObject(rentACarObjectData);
    res.status(201).json(newObject);
  } catch (error) {
    console.log("name", rentACarObjectData.objectName);
    res.status(500).json({ error: 'Unable to create a rent-a-car object.' });
  }
}
// Get all rent-a-car objects
function getRentACarObjects(req, res) {
  const objects = RentACarObjectModel.getRentACarObjects();
  res.status(200).json(objects);
}

// Get a rent-a-car object by ID
function getRentACarObjectById(req, res) {
  const objectId = req.params.id; // Assuming the object ID is passed as a URL parameter
  const object = RentACarObjectModel.getRentACarObjectById(objectId);
  
  if (!object) {
    res.status(404).json({ error: 'Rent-a-car object not found.' });
  } else {
    res.status(200).json(object);
  }
}

// Update a rent-a-car object by ID
function updateRentACarObjectById(req, res) {
  try {
    const objectId = req.params.id; // Assuming the object ID is passed as a URL parameter
    const updatedObjectData = req.body; // Updated object data sent in the request body
    const updatedObject = RentACarObjectModel.updateRentACarObject(objectId, updatedObjectData);
    
    if (!updatedObject) {
      res.status(404).json({ error: 'Rent-a-car object not found.' });
    } else {
      res.status(200).json(updatedObject);
    }
  } catch (error) {
    res.status(500).json({ error: 'Unable to update the rent-a-car object.' });
  }
}

// Delete a rent-a-car object by ID
function deleteRentACarObjectById(req, res) {
  try {
    const objectId = req.params.id; // Assuming the object ID is passed as a URL parameter
    const deletedObject = RentACarObjectModel.deleteRentACarObject(objectId);
    
    if (!deletedObject) {
      res.status(404).json({ error: 'Rent-a-car object not found.' });
    } else {
      res.status(204).send(); // No content, indicating successful deletion
    }
  } catch (error) {
    res.status(500).json({ error: 'Unable to delete the rent-a-car object.' });
  }
}

async function registerObject(req, res) {
  requireRole('admin'), (req, res) => {
    // This route can only be accessed by users with the 'admin' role
    res.json({ message: 'Admin-only route' })};
  try {
    // Get user data from the request body
    const { id, objectName, vehicle, objectStatus, businessHours, location, logo, rating,managerId} = req.body;

    

    // Create a new vehicle object
    const newObject = {
      id,
      objectName,
      vehicle,
      objectStatus,
      businessHours,
      location,
      logo,
      rating,
      managerId
    };

    // Create the user in your data store
    const createdObject = RentACarObjectModel.createRentACarObject(newObject);

    res.status(201).json({registered: true, message: 'Rent a car object registered successfully', rentACarObject: createdObject });
  } catch (error) {
    console.error('Registration error1:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

module.exports = {
  createRentACarObject,
  getRentACarObjects,
  getRentACarObjectById,
  updateRentACarObjectById,
  deleteRentACarObjectById,
  registerObject
};