const UserModel = require('../models/user_model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();

// Create a new user
function createUser(req, res) {
  try {
    const userData = req.body; 
    const newUser = UserModel.createUser(userData);
    res.status(201).json(newUser);
  } catch (error) {
    
    res.status(500).json({ error: 'Unable to create a user.' });
  }
}

// Get all users
function getUsers(req, res) {
  const users = UserModel.getUsers();
  res.status(200).json(users);
}

// Get a user by ID
function getUserById(req, res) {
  const userId = req.params.id; 
  const user = UserModel.getUserById(userId);
  
  if (!user) {
    res.status(404).json({ error: 'User not found.' });
  } else {
    res.status(200).json(user);
  }
}

// Update a user by ID
function updateUserById(req, res) {
  try {
    const userId = req.params.id; // Assuming the user ID is passed as a URL parameter
    const updatedUserData = req.body; // Updated user data sent in the request body
    const updatedUser = UserModel.updateUser(userId, updatedUserData);
    
    if (!updatedUser) {
      res.status(404).json({ error: 'User not found.' });
    } else {
      res.status(200).json(updatedUser);
    }
  } catch (error) {
    res.status(500).json({ error: 'Unable to update the user.' });
  }
}
// Delete a user by ID
function deleteUserById(req, res) {
  try {
    const userId = req.params.id; // Assuming the user ID is passed as a URL parameter
    const deletedUser = UserModel.deleteUser(userId);
    
    if (!deletedUser) {
      res.status(404).json({ error: 'User not found.' });
    } else {
      res.status(204).send(); // No content, indicating successful deletion
    }
  } catch (error) {
    res.status(500).json({ error: 'Unable to delete the user.' });
  }
}

async function login(req, res) {
  const { username, password } = req.body;
  require('dotenv').config();
  const secretKey = process.env.JWT_SECRET;
  

  try {
    // Validate User Input
    if (!username || !password) {
      return res.status(400).json({ error: 'Username and password are required' });
    }

    // Retrieve User Data by Username
    const user = UserModel.getUserByUsername(username);
    

    // Check Password
    if (!user) {
      return res.status(401).json({ error: 'Authentication failed' });
    }

    // Compare the provided password with the stored password hash
    bcrypt.compare(password, user.password, (err, result) => {
      if (err || !result) {
        return res.status(401).json({ error: 'Authentication failed' });
      }

      // Session or Token Creation
      const token = jwt.sign({ userId: user.id },secretKey, { expiresIn: '24h' });

      // Respond to the Client
      res.status(200).json({ authenticated: true, user: { username: user.username, role: user.role}, message: 'Login successful', token });
      
    });
  } catch (error) {
    console.error('Login error:', error);
    res.status(500).json({ authenticated: false, error: 'Internal server error' });
  }
}

async function register(req, res) {
  try {
    // Get user data from the request body
    const { id, username, password, firstname, lastname, gender, birthday, role } = req.body;

    // Check if the username already exists
    if (UserModel.getUserByUsername(username)) {
      return res.status(400).json({ error: 'Username already exists' });
    }

    // Hash the user's password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create a new user object
    const newUser = {
      id,
      username,
      password: hashedPassword,
      firstname,
      lastname,
      gender,
      birthday,
      role,
      hasRentACarObject: false, // You can set default values here
    };

    // Create the user in your data store
    const createdUser = UserModel.createUser(newUser);

    res.status(201).json({registered: true, message: 'User registered successfully', user: createdUser });
  } catch (error) {
    console.error('Registration error1:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

module.exports = {
  createUser,
  getUsers,
  getUserById,
  updateUserById,
  deleteUserById,
  login,
  register
};