const VehicleModel = require('../models/vehicle_model');
const sharp = require('sharp');


// Create a new vehicle
function createVehicle(req, res) {
  const vehicleData = req.body; 
  try {
    
    const newVehicle = VehicleModel.createVehicle(vehicleData);
    res.status(201).json(newVehicle);

  } catch (error) {
    console.log("name", vehicleData.vehicleMark);
    res.status(500).json({ error: 'Unable to create a vehicle.' });
  }
}


// Get all vehicles
function getVehicles(req, res) {
  const vehicles = VehicleModel.getVehicles();
  res.status(200).json(vehicles);
}

// Get a vehicle by ID
function getVehicleById(req, res) {
  const vehicleId = req.params.id; // Assuming the vehicle ID is passed as a URL parameter
  const vehicle = VehicleModel.getVehicleById(vehicleId);
  
  if (!vehicle) {
    res.status(404).json({ error: 'Vehicle not found.' });
  } else {
    res.status(200).json(vehicle);
  }
}


// Update a vehicle by ID
function updateVehicleById(req, res) {
  try {
    const vehicleId = req.params.id; // Assuming the vehicle ID is passed as a URL parameter
    const updatedVehicleData = req.body; // Updated vehicle data sent in the request body
    const updatedVehicle = VehicleModel.updateVehicle(vehicleId, updatedVehicleData);
    
    if (!updatedVehicle) {
      res.status(404).json({ error: 'Vehicle not found.' });
    } else {
      res.status(200).json(updatedVehicle);
    }
  } catch (error) {
    res.status(500).json({ error: 'Unable to update the vehicle.' });
  }
}


// Delete a vehicle by ID
function deleteVehicleById(req, res) {
  try {
    const vehicleId = req.params.id; // Assuming the vehicle ID is passed as a URL parameter
    const deletedVehicle = VehicleModel.deleteVehicle(vehicleId);
    
    if (!deletedVehicle) {
      res.status(404).json({ error: 'Vehicle not found.' });
    } else {
      res.status(204).send(); // No content, indicating successful deletion
    }
  } catch (error) {
    res.status(500).json({ error: 'Unable to delete the vehicle.' });
  }
}

async function registerVehicle(req, res) {
  try {
    //uzmi podatke iz requesta
    const { id, vehicleMark, vehiclePrice, vehicleType, rentacarobjectID, consumption, vehicleTransmissions, fuelType,doorNumber,peopleInCarNumber,description,image,status } = req.body;
    const compressedImage = await compressImage(image);
    

    // novi vehicle objekat
    const newVehicle = {
    id,
    vehicleMark,
    vehiclePrice,
    vehicleType,
    rentacarobjectID,
    consumption,
    vehicleTransmissions,
    fuelType,
    doorNumber,
    peopleInCarNumber,
    description,
    image: compressedImage,
    status
    };

    
    const createdVehicle = VehicleModel.createVehicle(newVehicle);

    res.status(201).json({registered: true, message: 'Vehicle registered successfully', vehicle: createdVehicle });
  } catch (error) {
    console.error('Registration error1:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

  async function compressImage(encodedImage) {
    try {
      const imageBuffer = Buffer.from(encodedImage, 'base64');
  
      const compressedImageBuffer = await sharp(imageBuffer)
        .resize({ width: 100, height: 100 }) 
        .toBuffer();
  
      return compressedImageBuffer.toString('base64');
    } catch (error) {
      console.error('Error compressing image:', error);
      throw new Error('Unable to compress image.');
    }
  }


module.exports = {
  createVehicle,
  getVehicles,
  getVehicleById,
  updateVehicleById,
  deleteVehicleById,
  registerVehicle
};