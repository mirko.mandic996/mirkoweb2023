const fs = require('fs');
const path = require('path');

const ordersFilePath = path.join(__dirname, '../data/orders.json');

// Define allowed values for the OrderStatus enum
const allowedOrderStatuses = ['obrada', 'odobreno', 'preuzeto', 'vraceno', 'odbijeno', 'otkazano']; 

// Function to read order data from the JSON file
function getOrders() {
  const ordersData = fs.readFileSync(ordersFilePath, 'utf8');
  return JSON.parse(ordersData);
}

// Function to write order data to the JSON file
function saveOrders(orders) {
  fs.writeFileSync(ordersFilePath, JSON.stringify(orders, null, 2), 'utf8');
}

// Create a new order
function createOrder(orderData) {
  const orders = getOrders();

  // Check if the provided order status is valid
  if (!allowedOrderStatuses.includes(orderData.orderStatus)) {
    throw new Error('Invalid order status');
  }

  const newOrder = {
    id: Date.now().toString(),
    vehicles: orderData.vehicles || [],
    objectId: orderData.objectId || 0,
    orderDateAndTime: orderData.orderDateAndTime ? new Date(orderData.orderDateAndTime) : null,
    orderDuration: orderData.orderDuration || '',
    orderPrice: orderData.orderPrice || 0,
    userId: orderData.userId || 0,
    orderStatus: orderData.orderStatus,
  };

  orders.push(newOrder);
  saveOrders(orders);
  return newOrder;
}

module.exports = { createOrder };
