const fs = require('fs');
const path = require('path');

const rentACarObjectsFilePath = path.join(__dirname, '../data/rentACarObjects.json');

// Define allowed values for the ObjectStatus enum
//const allowedObjectStatuses = ['Radi', 'Ne radi']; 

// Function to read rent a car object data from the JSON file
function getRentACarObjects() {
  try{
  const objectsData = fs.readFileSync(rentACarObjectsFilePath, 'utf8');
  return JSON.parse(objectsData);
} catch (error) {
  // Handle the error, e.g., by returning an empty array
  console.error('Error reading vehicle data:', error);
  return [];
}
}

// Function to write rent a car object data to the JSON file
function saveRentACarObjects(objects) {
  fs.writeFileSync(rentACarObjectsFilePath, JSON.stringify(objects, null, 2), 'utf8');
}

// Create a new rent a car object
function createRentACarObject(objectData) {
  const objects = getRentACarObjects();

  // // Check if the provided object status is valid
  // if (!allowedObjectStatuses.includes(objectData.objectStatus)) {
  //   throw new Error('Invalid object status');
  // }

  const newObject = {
    id: Date.now().toString(),
    objectName: objectData.objectName || '',
    vehicle: objectData.vehicle || [],
    objectStatus: objectData.objectStatus,
    businessHours: objectData.businessHours || '',
    location: objectData.location || {},
    logo: objectData.logo || '',
    rating: objectData.rating || 0,
    managerId: objectData.managerId || 0,
  };

  objects.push(newObject);
  saveRentACarObjects(objects);
  return newObject;
}

// Get a rent-a-car object by ID
function getRentACarObjectById(objectId) {
  const objects = getRentACarObjects();
  return objects.find(object => object.id === objectId);
}

// Update an existing rent-a-car object by ID
function updateRentACarObjectById(objectId, updatedObjectData) {
  const objects = getRentACarObjects();
  const objectIndex = objects.findIndex(object => object.id === objectId);

  if (objectIndex === -1) {
    return null; // Object not found, return null or throw an error
  }

  // Update object data with the provided values
  objects[objectIndex] = {
    ...objects[objectIndex],
    ...updatedObjectData,
    id: objectId, // Ensure the ID remains the same
  };

  saveRentACarObjects(objects);
  return objects[objectIndex];
}

// Delete an existing rent-a-car object by ID
function deleteRentACarObjectById(objectId) {
  const objects = getRentACarObjects();
  const objectIndex = objects.findIndex(object => object.id === objectId);

  if (objectIndex === -1) {
    return null; // Object not found, return null or throw an error
  }

  // Remove the object from the array
  const deletedObject = objects.splice(objectIndex, 1)[0];
  
  saveRentACarObjects(objects);
  return deletedObject;
}

module.exports = {
  createRentACarObject,
  getRentACarObjects,
  getRentACarObjectById,
  updateRentACarObjectById,
  deleteRentACarObjectById,
};
