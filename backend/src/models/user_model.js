const fs = require('fs');
const path = require('path');
const allowedRoles = ['korisnik', 'menadzer', 'admin'];
const usersFilePath = path.join(__dirname, '../data/users.json');

// Function to read user data from the JSON file
function getUsers() {
  try {
    const usersData = fs.readFileSync(usersFilePath, 'utf8');
    return JSON.parse(usersData);
  } catch (error) {
    // Handle the error, e.g., by returning an empty array
    console.error('Error reading user data:', error);
    return [];
  }

}

function getUserById(userId) {
  const users = getUsers();
  return users.find(user => user.id === userId);
}

function getManager() {
  const users = getUsers();
  const managers = users.filter(user => user.role === 'menadzer');

  return managers;
}

// Function to write user data to the JSON file
function saveUsers(users) {
  fs.writeFileSync(usersFilePath, JSON.stringify(users, null, 2), 'utf8');
}


// Create a new user
function createUser(userData){
  const users = getUsers();

  // // Check if the provided role is valid
  //  if (!allowedRoles.includes(userData.role)) {
  //  throw new Error('Invalid role');
  //  }
  
  const newUser = {
    id: Date.now().toString(), 
    username: userData.username || '',
    password: userData.password || '',
    firstname: userData.firstname || '',
    lastname: userData.lastname || '',
    gender: userData.gender || '',
    birthday: userData.birthday ? new Date(userData.birthday) : null, // Convert to Date object if birthday is provided
    role: userData.role || '',
    hasRentACarObject: userData.hasRentACarObject || false,
  };
  users.push(newUser);
  saveUsers(users);
  return newUser;
}

// Update an existing user by ID
function updateUser(userId, updatedUserData) {
  const users = getUsers();
  const userIndex = users.findIndex(user => user.id === userId);

  if (userIndex === -1) {
    return null; // User not found, return null or throw an error
  }

  // Update user data with the provided values
  users[userIndex] = {
    ...users[userIndex],
    ...updatedUserData,
    id: userId, // Ensure the ID remains the same
  };

  saveUsers(users);
  return users[userIndex];
}

// Delete an existing user by ID
function deleteUser(userId) {
  const users = getUsers();
  const userIndex = users.findIndex(user => user.id === userId);

  if (userIndex === -1) {
    return null; // User not found, return null or throw an error
  }

  // Remove the user from the array
  const deletedUser = users.splice(userIndex, 1)[0];
  
  saveUsers(users);
  return deletedUser;
}

function getUserByUsername(username) {
  const users = getUsers();
  return users.find(user => user.username === username);
}

module.exports = {
  createUser,
  getUsers,
  getUserById,
  updateUser,
  deleteUser,
  getUserByUsername,
  getManager
};
