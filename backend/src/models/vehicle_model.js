const fs = require('fs');
const path = require('path');
const base64 = require('base64-js');
const { createCanvas, loadImage } = require('canvas');

const vehiclesFilePath = path.join(__dirname, '../data/vehicles.json');

// Define allowed values for enums
const allowedVehicleTypes = ['auto', 'kombi', 'mobilehome']; 
const allowedVehicleTransmissions = ['automatik', 'manuelni'];
const allowedFuelTypes = ['benzin', 'diesel', 'elektricni']; 
const allowedVehicleAvailability = ['available', 'booked', 'maintenance', 'unavailable']; 

// Function to read vehicle data from the JSON file
function getVehicles() {
  try{
  const vehiclesData = fs.readFileSync(vehiclesFilePath, 'utf8');
  return JSON.parse(vehiclesData);
} catch (error) {
  // Handle the error, e.g., by returning an empty array
  console.error('Error reading vehicle data:', error);
  return [];
}
}

// Function to write vehicle data to the JSON file
function saveVehicles(vehicles) {
  fs.writeFileSync(vehiclesFilePath, JSON.stringify(vehicles, null, 2), 'utf8');
}

// Create a new vehicle
function createVehicle(vehicleData) {
  const vehicles = getVehicles();

  // Check if the provided vehicle type is valid
  // if (!allowedVehicleTypes.includes(vehicleData.vehicleType)) {
  //   throw new Error('Invalid vehicle type');
  // }

  // // Check if the provided vehicle transmission is valid
  // if (!allowedVehicleTransmissions.includes(vehicleData.vehicleTransmissions)) {
  //   throw new Error('Invalid vehicle transmission');
  // }

  // // Check if the provided fuel type is valid
  // if (!allowedFuelTypes.includes(vehicleData.fuelType)) {
  //   throw new Error('Invalid fuel type');
  // }

  // // Check if the provided vehicle availability status is valid
  // if (!allowedVehicleAvailability.includes(vehicleData.status)) {
  //   throw new Error('Invalid vehicle availability status');
  // }

  const newVehicle = {
    id: Date.now().toString(),
    vehicleMark: vehicleData.vehicleMark || '',
    vehiclePrice: vehicleData.vehiclePrice || 0,
    vehicleType: vehicleData.vehicleType || '',
    rentacarobjectID: vehicleData.rentacarobjectID || 0,
    consumption: vehicleData.consumption || 0,
    vehicleTransmissions: vehicleData.vehicleTransmissions || '',
    fuelType: vehicleData.fuelType || '',
    doorNumber: vehicleData.doorNumber || 0,
    peopleInCarNumber: vehicleData.peopleInCarNumber || 0,
    description: vehicleData.description || '',
    image: vehicleData.image || '',
    status: vehicleData.status || '',
  };

  vehicles.push(newVehicle);
  saveVehicles(vehicles);
  return newVehicle;
}


// Get a vehicle by ID
function getVehicleById(vehicleId) {
  const vehicles = getVehicles();
  return vehicles.find(vehicle => vehicle.id === vehicleId);
}

// Update an existing vehicle by ID
function updateVehicleById(vehicleId, updatedVehicleData) {
  const vehicles = getVehicles();
  const vehicleIndex = vehicles.findIndex(vehicle => vehicle.id === vehicleId);

  if (vehicleIndex === -1) {
    return null; // Vehicle not found, return null or throw an error
  }

  // Update vehicle data with the provided values
  vehicles[vehicleIndex] = {
    ...vehicles[vehicleIndex],
    ...updatedVehicleData,
    id: vehicleId, // Ensure the ID remains the same
  };

  saveVehicles(vehicles);
  return vehicles[vehicleIndex];
}

// Delete an existing vehicle by ID
function deleteVehicleById(vehicleId) {
  const vehicles = getVehicles();
  const vehicleIndex = vehicles.findIndex(vehicle => vehicle.id === vehicleId);

  if (vehicleIndex === -1) {
    return null; // Vehicle not found, return null or throw an error
  }

  // Remove the vehicle from the array
  const deletedVehicle = vehicles.splice(vehicleIndex, 1)[0];
  
  saveVehicles(vehicles);
  return deletedVehicle;
}

module.exports = {
  createVehicle,
  getVehicles,
  getVehicleById,
  updateVehicleById,
  deleteVehicleById,
};
