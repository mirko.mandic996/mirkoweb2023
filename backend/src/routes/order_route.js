const express = require('express');
const OrderController = require('../controllers/order_controller');

const router = express.Router();

// Create a new order
router.post('/orders', OrderController.createOrder);

// Define other order-related routes here

module.exports = router;
