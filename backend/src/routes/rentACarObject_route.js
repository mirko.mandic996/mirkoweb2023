const express = require('express');
const RentACarObjectController = require('../controllers/rentACarObject_controller');


const router = express.Router();

// Create a new rent-a-car object
router.post('/objects', RentACarObjectController.createRentACarObject);

router.post('/addObject', RentACarObjectController.registerObject);

// Get all rent-a-car objects
router.get('/objects', RentACarObjectController.getRentACarObjects);

// Get a rent-a-car object by ID
router.get('/objects/:id', RentACarObjectController.getRentACarObjectById);

// Update a rent-a-car object by ID
router.put('/objects/:id', RentACarObjectController.updateRentACarObjectById);

// Delete a rent-a-car object by ID
router.delete('/objects/:id', RentACarObjectController.deleteRentACarObjectById);

module.exports = router;
