const express = require('express');
const UserController = require('../controllers/user_controller');

const router = express.Router();

// Create a new user
router.post('/users', UserController.createUser);

// Get all users
router.get('/users', UserController.getUsers);

// Get a user by ID
router.get('/users/:id', UserController.getUserById);

// Update a user by ID
router.put('/users/:id', UserController.updateUserById);

// Delete a user by ID
router.delete('/users/:id', UserController.deleteUserById);

// Login route
router.post('/login', UserController.login);

// Register route
router.post('/register', UserController.register);

module.exports = router;