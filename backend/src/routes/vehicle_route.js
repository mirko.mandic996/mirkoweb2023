const express = require('express');
const VehicleController = require('../controllers/vehicle_controller');

const router = express.Router();

// Create a new vehicle
router.post('/addVehicle', VehicleController.registerVehicle);

router.post('/vehicles', VehicleController.createVehicle);

// Get all vehicles
router.get('/vehicles', VehicleController.getVehicles);

// Get a vehicle by ID
router.get('/vehicles/:id', VehicleController.getVehicleById);

// Update a vehicle by ID
router.put('/vehicles/:id', VehicleController.updateVehicleById);

// Delete a vehicle by ID
router.delete('/vehicles/:id', VehicleController.deleteVehicleById);


module.exports = router;
