// router.js
import Vue from 'vue';
import Router from 'vue-router';
import Login from './components/LoginForm.vue'; 
import Register from './components/RegisterUserForm.vue'; 
import RegisterVehicle from './components/RegisterVehicleForm.vue'; 
import RegisterObject from './components/RegisterObjectForm.vue';
import VehicleList from './components/VehicleList.vue';

Vue.use(Router);

const router =  new Router({
  routes: [
    {
      path: '/',
      redirect: '/login',
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
        path: '/register',
        name: 'register user',
        component: Register,
      },
      {
        path: '/addVehicle',
        name: 'addVehicle',
        component: RegisterVehicle,
      },

      {
        path: '/vehicles',
        name: 'Vehicles',
        component: VehicleList,
      },
      {
        path: '/addObject',
        name: 'addObject',
        component: RegisterObject,
        meta: { requiresAdmin: true }, 
        // beforeEnter: (to, from, next) => {
        //   // Check if the user is an admin (replace with your authentication logic)
        //   const userRole =  // Get the user's role
  
        //   if (userRole === 'admin') {
        //     next(); // User is an admin, allow access
        //   } else {
        //     // User is not an admin, redirect to another page or show an error
        //     next('/unauthorized'); // Redirect to an unauthorized page or route
        //   }
        // }
      },
    
  ],
  mode: 'history'

});

// router.beforeEach((to, from, next) => {
//   if (to.matched.some((record) => record.meta.requiresAdmin)) {
//     // Check the user's role (replace with your authentication logic)
//     const userRole = Register.userRole; // Replace with your authentication logic

//     if (userRole === 'admin') {
//       // User has the required admin role, allow access
//       next();
//     } else {
//       console.log("role",userRole);
//       //next('/unauthorized'); // Redirect to an unauthorized page or route
//     }
//   } else {
//     // Continue to the next route
//     next();
//   }
// });

export default router;



